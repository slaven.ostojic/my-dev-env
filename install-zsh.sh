#!/bin/bash

set -e

sudo apt install -y zsh
chsh -s "$(which zsh)"

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

zsh -c 'echo "ZSH installed. This is the first run. Please close this session with Ctrl+D."'

sed -i 's/^ZSH_THEME=.*/ZSH_THEME="essembeh"/' ~/.zshrc

echo "Done!"

gnome-session-quit
