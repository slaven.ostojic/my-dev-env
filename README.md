# My Dev Env

> This repository will contain some of my favorite tools and scripts which I use for my day-to-day work on Linux.

### Install

```shell
sh -c "$(curl -fsSL https://gitlab.com/slaven.ostojic/my-dev-env/-/raw/main/install.sh)"
```

### Content

`scripts/jdkswitch.sh`: to switch between installed JDK versions.
