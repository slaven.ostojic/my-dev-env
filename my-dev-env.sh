#!/bin/bash

# Use to init configuration.

autoload -U +X bashcompinit && bashcompinit

alias ll="ls -la"
alias jdkswitch='sudo sh $HOME/projects/private/my-dev-env/scripts/jdkswitch.sh'
alias xclip-c="xclip -selection clipboard"
alias xclip-p="xclip -o -selection clipboard"

export JAVA_HOME=/usr/lib/jvm/current
export JAVA_PATH=$JAVA_HOME/bin

export M2_HOME=/opt/maven
export M2_PATH=$M2_HOME/bin

export PATH=$JAVA_PATH:$M2_PATH:$PATH
