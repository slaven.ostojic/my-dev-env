#!/bin/bash

set -e

# All JDKs should be installed in $path_prefix directory.
# Symlinks for each version should be created, i.e.: "11 -> jdk-11.0.2"
# Most of JDKs can be found here: https://jdk.java.net/archive/

version=$1
current="current"
path_prefix="/usr/lib/jvm"
location="$path_prefix/$version"
pointer="$path_prefix/$current"

list_supported_versions() {
  for file in "$path_prefix"/*; do
    name=${file##*/}
    [ -L "$file" ] && [ "$name" != "$current" ] && echo "- $name"
  done
}

[ -z "$version" ] && echo "Please specify JDK version." && exit 1
[ ! -d "$location" ] && echo "Unsupported JDK version. Should be one of:" && echo "$(list_supported_versions)" && exit 2

rm "$pointer"
ln -s "$location" "$pointer"

echo "Done! JDK set to:"
$pointer/bin/java --version
