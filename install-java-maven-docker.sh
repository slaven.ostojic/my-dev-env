#!/bin/bash

set -e

# Install JDKs

curl https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz -o openjdk-11.tar.gz
curl https://download.java.net/java/GA/jdk17.0.2/dfd4a8d0985749f896bed50d7138ee7f/8/GPL/openjdk-17.0.2_linux-x64_bin.tar.gz -o openjdk-17.tar.gz
curl https://download.java.net/java/GA/jdk21/fd2272bbf8e04c3dbaee13770090416c/35/GPL/openjdk-21_linux-x64_bin.tar.gz -o openjdk-21.tar.gz

mkdir /usr/lib/jvm

sudo mv openjdk-11.tar.gz /usr/lib/jvm/.
sudo mv openjdk-17.tar.gz /usr/lib/jvm/.
sudo mv openjdk-21.tar.gz /usr/lib/jvm/.

cd /usr/lib/jvm

sudo tar xzvf openjdk-11.tar.gz
sudo tar xzvf openjdk-17.tar.gz
sudo tar xzvf openjdk-21.tar.gz

sudo ln -s jdk-11.0.2 11
sudo ln -s jdk-17.0.2 17
sudo ln -s jdk-21 21
sudo ln -s 11 current

sudo rm openjdk-11.tar.gz
sudo rm openjdk-17.tar.gz
sudo rm openjdk-21.tar.gz

# Install Maven

curl https://dlcdn.apache.org/maven/maven-3/3.9.2/binaries/apache-maven-3.9.2-bin.tar.gz -o maven.tar.gz
sudo mv maven.tar.gz /opt/.
cd /opt
sudo tar xzvf maven.tar.gz
sudo mv apache-maven-3.9.2 maven
sudo rm maven.tar.gz

# Install Docker

sudo apt install -y apt-transport-https ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-cache policy docker-ce
sudo apt install -y docker-ce
sudo systemctl status docker
sudo usermod -aG docker "${USER}"
su - "${USER}"
