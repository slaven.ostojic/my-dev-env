#!/bin/bash

set -e

name="my-dev-env"
home="$HOME/projects/private"
path="$home/$name"

sudo apt update && sudo dist-upgrade

sudo apt install -y curl vim build-essential git net-tools make xclip traceroute

. ./install-zsh.sh

mkdir -p "$home"
git clone git@gitlab.com:slaven.ostojic/my-dev-env.git "$path"
echo "source $path/$name.sh" >>"$HOME/.zshrc"

. ./install-java-maven-docker.sh

curl -L https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o google-chrome.deb
sudo apt install ./google-chrome.deb
rm google-chrome.deb

echo "Done!"
